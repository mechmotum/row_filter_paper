pdf: mainpdf coverletter response
mainpdf: firstpdf secondpdf
firstpdf: main.tex
	pdflatex main.tex
secondpdf: nomenclature bibliography main.tex main.bbl main.nls
	pdflatex main.tex
	pdflatex main.tex
nomenclature: main.tex main.nlo
	makeindex main.nlo -s nomencl.ist -o main.nls
bibliography: main.tex main.aux
	bibtex main.aux
coverletter: cover-letter.tex cover-letter-03.tex
	pdflatex cover-letter.tex
	pdflatex cover-letter-03.tex
response: review-response.tex review-response-02.tex
	pdflatex review-response.tex
	pdflatex review-response-02.tex
trackchanges:
	# This creates a diff between master and the first plos submission
	git checkout first-plos-submission
	cp main.tex first-plos-submission.tex
	git checkout master
	latexdiff first-plos-submission.tex main.tex > diff-master-first.tex
	rm first-plos-submission.tex
	pdflatex diff-master-first.tex
	makeindex diff-master-first.nlo -s nomencl.ist -o diff-master-first.nls
	bibtex diff-master-first.aux
	pdflatex diff-master-first.tex
	pdflatex diff-master-first.tex
	# This creates a diff between master and the third plos submission
	git checkout third-plos-submission
	cp main.tex third-plos-submission.tex
	git checkout master
	latexdiff third-plos-submission.tex main.tex > diff-master-third.tex
	rm third-plos-submission.tex
	pdflatex diff-master-third.tex
	makeindex diff-master-third.nlo -s nomencl.ist -o diff-master-third.nls
	bibtex diff-master-third.aux
	pdflatex diff-master-third.tex
	pdflatex diff-master-third.tex
mergepanelfigs:
	convert images/acc-vs-percent-stroke-elite.png images/acc-vs-percent-stroke-club.png +append images/acc-vs-percent-stroke.png
	convert images/speed-vs-percent-stroke-elite.png images/speed-vs-percent-stroke-club.png +append images/speed-vs-percent-stroke.png
	convert images/acc-vs-percent-stroke.png -font arial -pointsize 50 -draw "text 30,930 'a) Elite rower'" -pointsize 50 -draw "text 1605,930 'b) Club-level rower'" images/acc-vs-percent-stroke.png
	convert images/speed-vs-percent-stroke.png -font arial -pointsize 50 -draw "text 30,930 'a) Elite rower'" -pointsize 50 -draw "text 1605,930 'b) Club-level rower'" images/speed-vs-percent-stroke.png
replacebib: main.bbl main.tex
	sed -e '/\\bibliography{rowing}/{r main.bbl' -e 'd}' main.tex > main-with-bib.tex
clean:
	(rm -rf *.ps *.log *.dvi *.aux *.*% *.lof *.lop *.lot *.toc *.idx *.ilg *.ind *.bbl *.blg *.cpt *.tdo *.nlo *.nls *.out)
