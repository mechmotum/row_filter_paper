\documentclass{letter}

\signature{Jason K. Moore, PhD}
\address{
  Mechanical and Aerospace Engineering \\
  University of California, Davis \\
  Davis,CA 95817
}

\begin{document}

\begin{letter}{Ibrahim Hoteit \\ Academic Editor \\ PLOS ONE\\ \textbf{Subject:
  Response to Reviewers}}
\opening{Dear Sir:}

I am writing on behalf of the co-authors of ``Adaptive smartphone-based sensor
fusion for estimating competitive rowing kinematic metrics'' submitted for
publication to PLOS ONE.

We have made signifiant changes to the manuscript to address each of the
reviewers comments. In particular, we carefully addressed each of the noted
items from reviewer~\#3. We have improved our discussion of the figures for
reviewer~\#1. And lastly, we have provided statements regarding our opinion
that our work is not a marginal contribution to the literature for
reviewer~\#2. Below you will find detailed responses to the specific reviewer
comments.

\textbf{Editorial issues}

\begin{enumerate}
  % 1
  \item We have included a PDF version of the paper with the tracked changes.
  % 2
  \item We have gone through and checked the style requirements.
  % 3
  \item We have updated the Financial Disclosure to be more clear about
    Hegemony Technologies, LLC's involvement and included it in the cover
    letter.
  % 4
  \item We have updated the Competing Interests section to be more clear about
    Hegemony Technologies, LLC's involvement and included it in the cover
    letter.
  % 5
  \item The figures have been fully removed from the manuscript and included
    separately.
  % 6
  \item We have included a copyright release from Mapbox for the use of the
    satellite image.
  % 7
  \item We have obtained permission from participant, Seth Weil, to make use of
    his photograph.
\end{enumerate}

\textbf{Reviewer 1}

Reviewer 1 noted three items to address.

\begin{enumerate}
  \item Regarding the major weakness of the figures not being in the text of
    the paper, we followed the PLOS ONE's submission rule that states ``Do
    not include figures in the main manuscript file. Each figure must be
    prepared and submitted as an individual file.'' No changes were made in
    this regard.
  \item The only parameter we found to be prematurely used before definition
    was ``Central Error'' as noted by the reviewer. We left the definition
    after the table because it would disrupt the intended flow of the text to
    move the definitions before problem formulation. We added a citation to the
    paper that defines Central Error and a note that directs the reader to the
    next section in the table caption. Additionally, we do have a full table of
    symbol definitions that was erroneously absent from the submission. It can
    be seen in the posted preprint (https://engrxiv.org/nykuh) and has now been
    added back to the submission.
\end{enumerate}

\textbf{Reviewer 2}

Reviewer 2 noted two items to address.

\begin{enumerate}
  \item The reviewer noted that he/she believed the methods were not novel and
    that the paper cannot be published due to this. Our primary target
    audiences are the sports engineering and rowing communities. As shown in
    our ``Related Work'' section the successful use and quantification of these
    methods is very much novel to those communities. We provided the extensive
    review of prior art to accentuate this. Secondly, it is important to note
    that ``novelty'' is not present in PLOS ONE's publication criteria, which
    we perceive to mean that it is not a necessity.
  \item The reviewer stated that there is a new surge of techniques for working
    with smartphone data in navigation. We have done a thorough review of the
    immediately related works at the time of writing. We will leave a broader
    literature review to other aspiring researchers or future research from our
    group. We also would love to be informed of specific techniques that would
    work better than our approach.
\end{enumerate}

\textbf{Reviewer 3}

Reviewer 3 noted numerous items to address and requested major changes.

Response to the first four questions in which the reviewer responded
non-positively:

\begin{enumerate}
  \item The reviewer indicated ``partly'' to ``Is the manuscript technically
    sound, and do the data support the conclusions?''. We were not able to
    determine from the free form comments why this is indicated as ``partly''
    as there were no specifics on what conclusions were not supported by the
    data.
  \item The reviewer indicated ``No'' to ``Has the statistical analysis been
    performed appropriately and rigorously?''. We have clarified the paper with
    respect to the specific items in the free form comments that seemed to
    indicate a lack of rigor by adding text and figures.
  \item The reviewer indicated ``No'' to ``Is the manuscript presented in an
    intelligible fashion and written in standard English?''. We improved the
    grammar and clarity issues that the reviewer pointed out. Otherwise, we
    believe we have used standard English and that the writing is intelligible
    without further specific comments.
\end{enumerate}

Responses to the 14 enumerated general comments:

\begin{enumerate}
  % 1
  \item We added a paragraph to close the ``Related Work'' section which
    indicates the advantages of our method over the others presented in this
    section.
  % 2
  \item ``Physics-informed'' implies that the filter is designed using
    knowledge or information derived from the physics of the system. In our
    case, we include the bias term in the Kalman filter that specifically deals
    with the kinematics of the pitching boat whereas the Complementary filter
    has no such element. We deleted ``Physics-informed'' to lessen confusion.
  % 3
  \item Yes, we would recommend using a DGPS. Additionally, the methods
    suggested in the paper could also be applied to the DGPS data. But we
    desire to use the smartphone due to their ubiquity as a consumer product. A
    rower or rowing team can invest in a differential GPS system but each rower
    is likely to already have a smartphone on their person. The use of DGPS
    will add significant cost and require additional effort in setting up,
    which is much less appealing to most rowers. We've added a sentence in the
    last paragraph of the discussion to this regard.
  % 4
  \item We added definitions to symbols in equation (4) in the relevant text as
    well as other minor improvements to the clarity of that section. Note also
    that our nomenclature section was erroneously absent from the submitted
    paper and is now included in the revised version.
  % 5
  \item We added these subscripts in some locations but left them off of any
    equation that is redundantly used for the three estimates: SP, CF, and KF
    as it would require tripling the number of equations presented.
  % 6
  \item Equations 1-3 and 6-11 all take sums over the number of time samples
    and are consistently using the variable n.
  % 7
  \item The real-time algorithms have a trivial number of FLOPS that modern
    smartphone processors can calculate many more orders of magnitude of.
    We noted the number of FLOPS for the real-time algorithm in the paper and
    added a section explaining this fact. Our extraneous target is smartphone
    implementation but we did not do so for the purposes of the paper due to
    the this triviality.
  % 8
  \item We changed this to ``As described in the smartphone acceleration
    section''.
  % 9
  \item We added the derivation and presentation of the non-linear form of the
    longitudinal acceleration as a function of the accelerometer readings and
    made the linear approximation step more clear.
  % 10
  \item We added a new plot (now Figure 6) showing the effects of low and
    high-pass filtering the integrated accelerometer and additional text
    indicating how the filter is applicable to our data.
  % 11
  \item We expanded the explanation of the bias term and the relationship to
    the pitching kinematics.
  % 12
  \item We use the scalar values $x$ and $y$ (non-bold) for the planar
    Cartesian coordinates of a point. We use the vector values $\mathbf{x}$ and
    $\mathbf{y}$ (bold) for the state and output vectors in the Kalman Filter
    formulation. We believe that this is sufficient distinction. Additionally,
    note that the nomenclature section of the paper was erroneously missing
    from the original submission. We hope that it improves the clarity of the
    selected symbols.
  % 13
  \item Rowing races at amateur and professional levels typically range from
    1000-5000 meters in length and are completed in timeframes that range from
    3 minutes to 20+ minutes.  Every boat before a race will execute an
    extensive warmup involving many hundreds and probably thousands of strokes
    over 30+ minutes.  This warmup period provides ample opportunity to
    complete all of the filter convergence for this implementation so that it
    will be optimally tuned and operational for the totality of a race.
  % 14
  \item We believe the description of the perceived ``basic components'' are
    important for our target audience, as these are not common discourse in the
    sports engineering literature. We also believe we have focused on detailing
    the novelty and applicability of our methods to a particular problem. The
    findings presented in the results section are all that we believe are
    supported by the data. If the reviewer has specific suggestions as to what
    other results can be derived from the data we would consider them. As it
    stands we do not have more results that are firmly supported by the data.
\end{enumerate}

Responses to the additional 5 enumerated comments:

\begin{enumerate}
  % 1
  \item We changed ``His'' to the reviewer's suggestion of appropriate
    language.
  % 2
  \item We change the sentence to the reviewer's suggested form.
  % 3
  \item Grammar mistake corrected.
  % 4
  \item Spelling corrected.
  % 5
  \item We collectively have written hundreds of published research papers and
    do not consider our current paper a casual technical report. We believe the
    paper as written meets PLOS ONE's publication criteria in that we are
    reporting original research, our experiments and analysis are performed to
    a high technical standard and described in detail, and our conclusions are
    supported by the data. We are happy to correct any portion that is
    incorrect or where the language is not ``clear, correct, and unambiguous''.
    We appreciate the style comments and have adjusted the paper with some of
    them.
\end{enumerate}


\textbf{Other updates}

We also incorporated some other changes that the reviewers should be aware.

\begin{enumerate}
  \item Figures with time on the abscissa have been enhanced with indicators of
    the stroke start/stop.
  \item The DGPS unit is advertised to sample at 10 Hz and initial testing
    indicated that was possible, but on review of the data we realized that our
    data was sampled an average of 5 Hz. We have adjusted the language in the
    paper to reflect that.
  \item The statistics were rewritten in terms of standard deviation instead of
    variance for consistency.
  \item The data repository has been updated to version 2 with more metadata.
\end{enumerate}

\textbf{Conclusion}

We have revised the manuscript and incorporated changes to address most
statements by the reviewers. We stand by our results and believe they are
backed by the data. We believe that the revised manuscript meets PLOS ONE's
publication criteria.

\closing{Sincerely,}

\end{letter}

\end{document}
