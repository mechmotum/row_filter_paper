# Introduction

This repository contains the source files for the paper:

> Cloud, B., Tarien, B., Liu, A., Shedd, T., Lin, X., Hubbard, M., Crawford, R.
P., & Moore, J. K. (2019). Adaptive smartphone-based sensor fusion for
estimating competitive rowing kinematic metrics.

The paper is currently under review at PLoS One.

The preprint is available at: https://doi.org/10.31224/osf.io/nykuh

This repository is a synchronized clone of the Overleaf project:

https://www.overleaf.com/1182623261hnfgrxdnzfny

A companion repository containing the software that generates the paper results
is at:

https://gitlab.com/mechmotum/row_filter

The data used in the analysis is available at: https://doi.org/10.6084/m9.figshare.7963643.v2

# License

The files in the repository are licensed under the Creative Common Attribution
4.0 license. If you distribute a copy or modified copy you must cite this
repository, its authors, and the license.
